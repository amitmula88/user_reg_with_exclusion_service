### user_reg_with_exclusion_service ###

to start the server :

```sh
$ ./start.sh
```

sample curl command to successfully register an user:

```sh
curl -X POST \
  http://localhost:9090/user/register \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
   "userName":"someuser",
   "password":"some_pa55W@rd",
   "dateOfBirth":"1815-12-11",
   "ssn":"123456789"
}'
```
