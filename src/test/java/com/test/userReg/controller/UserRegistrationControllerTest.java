package com.test.userReg.controller;

import com.test.userReg.model.User;
import com.test.userReg.repository.UserRepository;
import com.test.userReg.service.ExclusionService;
import com.test.userReg.service.RegistrationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.text.ParseException;
import java.time.LocalDate;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebMvcTest(UserRegistrationController.class)
public class UserRegistrationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RegistrationService registrationService;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private ExclusionService exclusionService;

    @Test
    public void testRegisterUserSuccess() throws Exception {
        when(exclusionService.validate(any(LocalDate.class), anyString())).thenReturn(true);
        when(registrationService.ifAlreadyRegisteredUser(any(User.class))).thenReturn(false);
        when(registrationService.registerUser(any(User.class))).thenReturn(1L);
        RequestBuilder requestBuilder = MockMvcRequestBuilders
            .post("/user/register")
            .contentType("application/json")
            .accept("application/json")
            .content("{\"userName\":\"someuserName\",\"password\":\"somePa55w@rd\",\"dateOfBirth\":\"2019-04-12\",\"ssn\":\"some_ssn\"}");
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        String expected = "{\"id\":\"1\",\"message\":\"User registered successfully!\"}";
        assertEquals(expected, result.getResponse().getContentAsString());
    }

    @Test
    public void testRegisterUserExclusionFailure() throws ParseException {
        when(exclusionService.validate(any(LocalDate.class), anyString())).thenReturn(false);
        RequestBuilder requestBuilder = MockMvcRequestBuilders
            .post("/user/register")
            .contentType("application/json")
            .accept("application/json")
            .content("{\"userName\":\"someuserName\",\"password\":\"somePa55w@rd\",\"dateOfBirth\":\"2019-04-12\",\"ssn\":\"some_ssn\"}");
        try {
            MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        } catch(Exception ex) {
            assertEquals("Request processing failed; nested exception is java.lang.Exception: User is not allowed to register!", ex.getMessage());
        }
    }

    @Test
    public void testRegisterUserAlreadyExistsFailure() throws ParseException {
        when(exclusionService.validate(any(LocalDate.class), anyString())).thenReturn(true);
        when(registrationService.ifAlreadyRegisteredUser(any(User.class))).thenReturn(true);
        RequestBuilder requestBuilder = MockMvcRequestBuilders
            .post("/user/register")
            .contentType("application/json")
            .accept("application/json")
            .content("{\"userName\":\"someuserName\",\"password\":\"somePa55w@rd\",\"dateOfBirth\":\"2019-04-12\",\"ssn\":\"some_ssn\"}");
        try {
            MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        } catch(Exception ex) {
            assertEquals("Request processing failed; nested exception is java.lang.Exception: User is already registered!", ex.getMessage());
        }
    }
}