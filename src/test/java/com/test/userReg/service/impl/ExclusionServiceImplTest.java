package com.test.userReg.service.impl;

import com.test.userReg.model.User;
import com.test.userReg.service.ExclusionService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.LocalDate;
import java.util.ArrayList;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

@RunWith(SpringJUnit4ClassRunner.class)
public class ExclusionServiceImplTest {

    ExclusionService exclusionService;

    @Before
    public void init() {
        exclusionService = new ExclusionServiceImpl();
        ReflectionTestUtils.setField(exclusionService, "excludedUsers", new ArrayList<User>(){{
            add(new User("", "", LocalDate.of(1815, 12, 10), "85385075"));
        }});
    }

    @Test
    public void testValidateSuccess() {
        boolean validate = exclusionService.validate(LocalDate.now(), "some_ssn");
        assertTrue(validate);
    }

    @Test
    public void testValidateFailure() {
        boolean validate = exclusionService.validate(LocalDate.of(1815, 12, 10), "85385075");
        assertFalse(validate);
    }
}
