package com.test.userReg.service.impl;

import com.test.userReg.entity.UserEntity;
import com.test.userReg.model.User;
import com.test.userReg.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Example;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RegistrationServiceImplTest {

    @InjectMocks
    RegistrationServiceImpl registrationService;

    @Mock
    UserRepository userRepository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testRegisterUser() {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(1l);
        when(userRepository.save(any(UserEntity.class))).thenReturn(userEntity);
        assertEquals(registrationService.registerUser(mock(User.class)), 1l);
    }

    @Test
    public void testIfAlreadyRegisteredUser() {
        when(userRepository.findAll(any(Example.class))).thenReturn(Collections.EMPTY_LIST);
        assertFalse(registrationService.ifAlreadyRegisteredUser(mock(User.class)));
        ArrayList<UserEntity> users = new ArrayList<>();
        UserEntity userEntity = new UserEntity();
        userEntity.setIsExcluded(true);
        users.add(userEntity);
        when(userRepository.findAll(any(Example.class))).thenReturn(users);
        assertTrue(registrationService.ifAlreadyRegisteredUser(mock(User.class)));
    }
}