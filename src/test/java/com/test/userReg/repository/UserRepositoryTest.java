package com.test.userReg.repository;

import com.test.userReg.entity.UserEntity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static org.junit.Assert.assertFalse;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void whenSaved_thenFindsByUserName() {
        UserEntity userEntity = new UserEntity();
        userEntity.setUserName("some_userName");
        userEntity.setPassword("some_password");
        userEntity.setDateOfBirth(LocalDate.now());
        userEntity.setSsn("some_ssn");
        userEntity.setIsExcluded(false);
        userRepository.save(userEntity);
        assertFalse(userRepository.findByUserName("some_userName").isEmpty());
    }
}