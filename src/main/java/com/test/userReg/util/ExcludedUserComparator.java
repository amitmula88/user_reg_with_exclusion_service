package com.test.userReg.util;

import com.test.userReg.model.User;

public class ExcludedUserComparator {

    public static boolean compare(User user1, User user2) {
        if(user1.getSsn().equals(user2.getSsn()) && user1.getDateOfBirth().equals(user2.getDateOfBirth())) return true;
        return false;
    }

} 