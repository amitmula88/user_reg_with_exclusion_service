package com.test.userReg.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "users")
@Data
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "username", nullable = false, updatable = false)
    private String userName;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "dob", nullable = false)
    private LocalDate dateOfBirth;

    @Column(name = "ssn", nullable = false, updatable = false)
    private String ssn;

    @Column(name = "excluded", nullable = true, updatable = true)
    private Boolean isExcluded;
}
