package com.test.userReg.service;

import com.test.userReg.model.User;

public interface RegistrationService {

    long registerUser(User user);

    boolean ifAlreadyRegisteredUser(User user);
}
