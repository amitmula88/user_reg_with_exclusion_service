package com.test.userReg.service.impl;

import com.test.userReg.entity.UserEntity;
import com.test.userReg.model.User;
import com.test.userReg.repository.UserRepository;
import com.test.userReg.service.RegistrationService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

@Service
public class RegistrationServiceImpl implements RegistrationService {

    @Autowired
    UserRepository userRepository;
    
    @Override
    public long registerUser(User user) {
        UserEntity userToPersist = new UserEntity();
        BeanUtils.copyProperties(user, userToPersist);
        userToPersist.setIsExcluded(false);
        return userRepository.save(userToPersist).getId();
    }

    @Override
    public boolean ifAlreadyRegisteredUser(User user) {
        UserEntity existingUser = new UserEntity();
        existingUser.setDateOfBirth(user.getDateOfBirth());
        existingUser.setSsn(user.getSsn());
        existingUser.setIsExcluded(false);
        Example<UserEntity> example = Example.of(existingUser, ExampleMatcher.matchingAll());
        return !userRepository.findAll(example).isEmpty();
    }
}
