package com.test.userReg.service.impl;

import com.test.userReg.model.User;
import com.test.userReg.service.ExclusionService;
import com.test.userReg.util.ExcludedUserComparator;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class ExclusionServiceImpl implements ExclusionService {

    List<User> excludedUsers;

    @PostConstruct
    private void init() {
        excludedUsers = new ArrayList<>();
        excludedUsers.add(new User("adaLovelace", "Analytical3ngineRulz", LocalDate.of(1815, 12, 10), "85385075"));
        excludedUsers.add(new User("alanTuring", "eniGmA123", LocalDate.of(1912, 06, 23), "123456789"));
        excludedUsers.add(new User("konradZuse", "zeD1", LocalDate.of(1910, 06, 22), "987654321"));
    }

    @Override
    public boolean validate(LocalDate dateOfBirth, String ssn) {
        User userToSearch = new User();
        userToSearch.setDateOfBirth(dateOfBirth);
        userToSearch.setSsn(ssn);
        return excludedUsers.stream().filter(eu -> ExcludedUserComparator.compare(eu, userToSearch)).count() > 0 ? false : true;
    }
}
