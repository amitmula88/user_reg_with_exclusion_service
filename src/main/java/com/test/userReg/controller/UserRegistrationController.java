package com.test.userReg.controller;

import com.test.userReg.model.User;
import com.test.userReg.service.ExclusionService;
import com.test.userReg.service.RegistrationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.Map;

@RestController
public class UserRegistrationController {

    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    RegistrationService registrationService;

    @Autowired
    ExclusionService exclusionService;

    @PostMapping(value = "/user/register", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String,String>> registerUser(@RequestBody @Valid User user) throws Exception {
        if(!exclusionService.validate(user.getDateOfBirth(), user.getSsn())) {
            throw new Exception("User is not allowed to register!");
        } else if (registrationService.ifAlreadyRegisteredUser(user)) {
            throw new Exception("User is already registered!");
        }
        long userId = registrationService.registerUser(user);
        Map<String, String> responseMap = new HashMap<>();
        responseMap.put("id", String.valueOf(userId));
        responseMap.put("message", "User registered successfully!");
        return new ResponseEntity(responseMap, HttpStatus.CREATED);
    }
}
